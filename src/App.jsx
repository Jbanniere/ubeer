import './App.css'
import Router from "./components/Router.jsx"
import { BrowserRouter } from "react-router-dom"
import Footer from "./components/Footer.jsx"
import Header from "./components/Header.jsx"
//import './slick.css';
//import './slick-theme.css';


function App() {
    return (
        <BrowserRouter>
          <Header />
          <Router />
          <Footer />
        </BrowserRouter>
    )
}

export default App;