import Home from "../components/Home";
import Error404 from "../components/Error404";
import Register from "../components/Register";

const routes = [
    {path:"/", component:<Home />},
    {path:"/*", component:<Error404 />},
    {path:"/register", component:<Register />},
]

export default routes;