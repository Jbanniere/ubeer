/*import { useState, useEffect, useContext } from "react"
import { useParams } from "react-router-dom"
import axios from "axios"
import { StoreContext } from "../tools/context.js"*/
import { useState } from "react"


const UpdateUser = () => {
    //const [ , setUpdateUser] = useState(null)
    //const [state,  ] = useContext(StoreContext)
    //const {id} = useParams()
    const [isValidated,  ] = useState(false)
    const [updateUser, setUpdateUser] = useState({
            id : 1,
            nom : 'Doe',
            prenom : 'John',
            street : '1 rue de la Paix',
            zip : '44000',
            city : 'Nantes',
            email : 'nom@gmail.com',
            role : 2
    })
       
    /*useEffect(() => {
        if(state.user.isAdmin || id == state.user.id){
            axios.post(`base/getUserById`,{id})
                .then(res => setUpdateUser(res.data.result.result[0]))
                .catch(err => console.log(err))
        }
    }, [id])*/
    
    
     const handleChange = (e) => {
        const {name, value} = e.target
        setUpdateUser({...updateUser, [name]: value})
    }
    
    /*// Pour valider la modification du user
    const submit = (e) =>{
        e.preventDefault()
        axios.post(`base/updateUser`,{...updateUser})
            .then(res => { 
                setIsValidated(true)
            })
            .catch(err => console.log(err))
    }*/

   return(
        <div>
            <h1>Modifier les Infos</h1>
                        <form type="submit">
                <fieldset>
                <legend>Infos à modifier</legend>
                <div className="fields">
                    <label>Rôle :</label>
                        <select className="input-size" id="role" name="role" onChange={handleChange} value={updateUser.role}>
                            <option value="1">Admin</option>
                            <option value="2">Brasserie</option>
                            <option value="3">User</option>
                        </select>
                    </div>
                    <div className="fields">
                        <label>Nom : </label>
                        <input className="input-size" type='text' name='nom' onChange={handleChange} value={updateUser.nom} />
                    </div>
                    <div className="fields">
                        <label>Prénom : </label>
                        <input className="input-size" type='text' placeholder='prénom' name='prenom' onChange={handleChange} value={updateUser.prenom} />
                    </div>
                    <div className="fields">
                        <label>Numéro et Rue : </label>
                        <input className="input-size" type='text' placeholder='street' name='street' onChange={handleChange} value={updateUser.street} />
                    </div>
                    <div className="fields">
                        <label>Code Postal : </label>
                        <input className="input-size" type='number' placeholder='zip' name='zip' onChange={handleChange} value={updateUser.zip} />
                    </div>
                    <div className="fields">
                        <label>Ville : </label>
                        <input className="input-size" type='text' placeholder='city' name='city' onChange={handleChange} value={updateUser.city} />
                    </div>
                    <div className="fields">
                        <label>Email : </label>
                        <input className="input-size" type='email' placeholder='email' name='email' onChange={handleChange} value={updateUser.email} />
                    </div>
                    <div className="btn-input">
                        <button /*onClick={submit}*/>Modifier les infos</button>
                    </div>
                </fieldset>
            </form>   
            
        {isValidated && (
            <p className="error-txt">Votre modification a bien été prise en compte</p>
        )}
        </div>
       )
}

export default UpdateUser