import { useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const images = [
  '/images/whiteBeer2.png',
  '/images/blondBeer.png',
  '/images/ipaBeer2.png'
];

const CustomCarousel = () => {
  const [zoomedIndex, setZoomedIndex] = useState(null);

  const handleClick = (index) => {
    setZoomedIndex(zoomedIndex === index ? null : index);
  };

  return (
    <Carousel
      showThumbs={false}
      centerMode = {true}
      centerSlidePercentage={33.33}
      infiniteLoop
      selectedItem={zoomedIndex}
      onClickItem={handleClick}
    >
      {images.map((src, index) => (
        <div
          key={index}
          className={zoomedIndex === index ? 'zoomed' : ''}
        >
          <img src={src} alt={`Slide ${index}`} />
        </div>
      ))}
    </Carousel>
  );
};

export default CustomCarousel;
