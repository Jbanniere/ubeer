import { NavLink } from "react-router-dom";
import { Fragment } from "react"
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";
import { useAuth0 } from '@auth0/auth0-react';

const NavBar = () => {

    const { isAuthenticated } = useAuth0();

    return(
        <Fragment>
          <nav>
            <ul style={{ listStyleType: "none", display: "flex", justifyContent: "space-around" }}>
                <li>
                    <img className="nav-logo-img" src="../images/logo 1.png" alt="logo de ubeer"/>
                </li>
                <li>
                    <NavLink to="/" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Accueil</NavLink>
                </li>
                <li>
                    <NavLink to="/aboutUs" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>À Propos</NavLink>
                </li>
                <li>
                    <NavLink to="/allBrasseries" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Nos Brasseries</NavLink>
                </li>
                <li>
                    <NavLink to="/allBieres" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Nos Bières</NavLink>
                </li>
                <li>
                    <NavLink to="/contactUs" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Nous Contacter</NavLink>
                </li>
                <li>
                    <NavLink to="/profil" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Mon Profil</NavLink>
                </li>
                <li>
                    <NavLink to="/allUsers" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Admin_Users</NavLink>
                </li>
                <li>
                    <NavLink to="/allMessages" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Admin_Messagerie</NavLink>
                </li>
                <li>
                    <NavLink to="/addBeers" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Brass_Ajouter Beer</NavLink>
                </li>
                <li>
                    <NavLink to="/brassAllBeers" style={({ isActive }) => ({ color: isActive ? 'green' : 'white' })}>Brass_All Beer</NavLink>
                </li>
                {!isAuthenticated && (
                      <li>
                          <LoginButton />
                      </li>
                    )}
                {isAuthenticated && (
                      <li>
                          <LogoutButton />
                      </li>
                    )}
            </ul>
          </nav>
        </Fragment>
     )
}

export default NavBar