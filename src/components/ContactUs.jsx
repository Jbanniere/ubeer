import { useState } from "react"
//import axios from "axios"
import { checkIsEmpty } from "../tools/checkInputEmpty.js"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPhone, faEnvelope, faLocationDot } from "@fortawesome/free-solid-svg-icons"


const ContactUs = () => {
    const [isValidated, ] = useState(false)
    const [errors, setErrors] = useState({})
    const [newContact, setNewContact] = useState({
        nom:'',
        prenom:'',
        email:'',
        objet: '',
        message:''
    })
    
    
    const handleChange = (e) => {
        const {name, value} = e.target
        setNewContact({...newContact,[name]:value})
    }
    
    const submit = (e) => {
        e.preventDefault()
        const empty = checkIsEmpty(newContact)
        if (Object.keys(empty).length > 0) {
            setErrors(empty);
            return
        }
         //Je vérifie si le mail est au bon format
        if (!/\S+@\S+\.\S+/.test(newContact.email)) {
            setErrors ({email : "Veuillez entrer une adresse email valide."})
            return
        }
        /*axios.post(`${BASE_URL}/contactUs`, {
            nom:newContact.nom,
            prenom:newContact.prenom,
            email:newContact.email,
            objet:newContact.objet,
            message:newContact.message
        })
    
        .catch(err => console.log(err))*/
    }
    
    return (
        <div className="container">
            <h1 className="title">Nous contacter</h1>
    
            {/* Contact Container */}
            <div className="contact-container">
                <img className="contact-img" src="./images/contactImg.webp" alt="Contact" />
                <div className="contact-address">
                    <div>
                        <FontAwesomeIcon icon={faPhone} />
                        <div>
                            <a href="tel:0233445566">02 33 44 55 66 </a>(du lundi au vendredi de 9h à 18h)
                        </div>
                    </div>
                    <div>
                        <FontAwesomeIcon icon={faEnvelope} />
                        <p>Utiliser le formulaire de contact ci-dessous</p>
                    </div>
                    <div>
                        <FontAwesomeIcon icon={faLocationDot} />
                        <div>
                            <p>UBEER</p>
                            <p>60 Rue du Général</p>
                            <p>CS 010203</p>
                            <p>75001 PARIS Cedex</p>
                        </div>
                    </div>
                </div>
            </div>
    
            {/* Form Container */}
            <form onSubmit={submit}>
                <fieldset>
                    <legend>Mes coordonnées</legend>
                    <div className="fields">
                        <label>Nom: </label>
                        <input
                            className="input-size"
                            type="text"
                            placeholder="Nom"
                            name="nom"
                            onChange={handleChange}
                            value={newContact.nom}
                        />
                        {errors.nom && <p>{errors.nom}</p>}
                    </div>
                    <div className="fields">
                        <label>Prénom: </label>
                        <input
                            className="input-size"
                            type="text"
                            placeholder="Prénom"
                            name="prenom"
                            onChange={handleChange}
                            value={newContact.prenom}
                        />
                        {errors.prenom && <p>{errors.prenom}</p>}
                    </div>
                    <div className="fields">
                        <label>Email: </label>
                        <input
                            className="input-size"
                            type="email"
                            placeholder="Email"
                            name="email"
                            onChange={handleChange}
                            value={newContact.email}
                        />
                        {errors.email && <p>{errors.email}</p>}
                    </div>
                </fieldset>
    
                <fieldset>
                    <legend>Ma Demande</legend>
                    <div className="fields">
                        <label>Objet de ma demande: </label>
                        <input
                            className="input-size"
                            type="text"
                            placeholder="Objet"
                            name="objet"
                            onChange={handleChange}
                            value={newContact.objet}
                        />
                        {errors.objet && <p>{errors.objet}</p>}
                    </div>
                    <div className="fields">
                        <label>Message: </label>
                        <textarea
                            placeholder="Message"
                            name="message"
                            onChange={handleChange}
                            value={newContact.message}
                        />
                        {errors.message && <p>{errors.message}</p>}
                    </div>
                    <div className="btn-input">
                        <button className="btn-form" type="submit">Envoyer votre demande</button>
                    </div>
                </fieldset>
            </form>
    
            {/* Success Message */}
            {isValidated && (
                <p className="error-txt">
                    Votre demande a bien été envoyée, nous mettons tout en œuvre pour vous répondre rapidement. À bientôt !
                </p>
            )}
        </div>
    );
}

export default ContactUs