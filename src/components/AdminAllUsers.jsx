//import { useEffect, useState } from "react"
// import axios from "axios"
import { NavLink } from "react-router-dom"
import { faPen } from '@fortawesome/free-solid-svg-icons/faPen'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from "@fortawesome/free-solid-svg-icons"


const AdminAllUsers = () => {

    const allUsers = [
        {
            id : 1,
            nom : 'Doe',
            prenom : 'John',
            street : '1 rue de la Paix',
            zip : '44000',
            city : 'Nantes',
            email : 'nom@gmail.com',
            role : 'brasserie'
        },
        {
            id : 1,
            nom : 'Doe',
            prenom : 'John',
            street : '1 rue de la Paix',
            zip : '44000',
            city : 'Nantes',
            email : 'nom@gmail.com',
            role : 'user'
        }
    ]

    /*const [ allUsers, setAllUsers ] = useState([])
    
    useEffect(() => {
        axios.get(`${BASE_URL}/getAllUsers`)
            .then(res => setAllUsers(res.data.result.result))
            .catch(err => console.log(err))
    },[])
    
    
    const deleteUser = (id) => {
        axios.post(`${BASE_URL}/deleteUser`)
        .then(res => setAllUsers(allUsers.filter((e)=> e.id !== id)))
        .catch(err => console.log(err))
    }*/

    return(
        <div className="users">
            <div className="title">
                <h1>Mes Utilisateurs</h1>
            </div>
            <table className="admin-users">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Rôle</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Rue</th>
                        <th>CP</th>
                        <th>Ville</th>
                        <th>Email</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                </thead>
                <tbody>
                    {allUsers.map((user,i) => {
                        return(
                        <tr key={i}>
                            <td>{user.id}</td>
                            <td>
                                <select value={user.role}>
                                    <option value="brasserie">Brasserie</option>
                                    <option value="user">User</option>
                                    <option value="admin">Admin</option>
                                </select>
                            </td>
                            <td>{user.nom}</td>
                            <td>{user.prenom}</td>
                            <td>{user.street}</td>
                            <td>{user.zip}</td>
                            <td>{user.city}</td>
                            <td>{user.email}</td>
                            <td><button className="btn-array fapen"><NavLink to={`/updateUser/${user.id}`}><FontAwesomeIcon icon={faPen} /></NavLink></button></td>
                            <td><button className="btn-array fatrash"><FontAwesomeIcon icon={faTrash}/></button></td>
                        </tr>
                        ) 
                    })}
                 </tbody>
            </table>
        </div>
    )
}

export default AdminAllUsers ;