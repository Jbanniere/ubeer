import { Fragment } from "react"
import Carousel from "./Carousel"

const Home = () => {

    return(
        <Fragment>
            <div className="homePage_top">
                <div className="homePage_text">
                    <img className="homePage_logo" src="../images/logo 1.png" alt="logo de ubeer"/>
                    <p>Service de livraisons Express de bières à domicile </p>
                </div>
                <img className = "homePage_img" src="../images/scooterHome.png"></img>
            </div>
            <div className="title">
                <h1>Nos Bières</h1>
            </div>
            <Carousel />

        </Fragment>
        )
}

export default Home