const Error404 = () => {
    return(
        <div className="error404">
            <p className="error-txt">Oups ! Désolée, page introuvable ...</p>
            <img src ="../images/brokenBeer.webp"></img>
        </div>
        )
}

export default Error404