//import { useEffect, useState } from "react"
//import axios from "axios"
//import { BASE_URL } from '../tools/constante.js'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faTrash } from "@fortawesome/free-solid-svg-icons"
//import { formatDate } from "../tools/date.js"

const AdminAllMessages = () => {
    const allContact = [
        {
            date : '28/05/2024',
            objet: 'Demande infos',
            message: 'Bonjour',
            nom: 'Doe',
            prenom: 'John',
            email: 'nom@gmail.com',
            etat: false
        },
        {
            date : '28/05/2024',
            objet: 'Demande infos',
            message: 'Bonjour',
            nom: 'Doe',
            prenom: 'John',
            email: 'nom@gmail.com',
            etat: false
        }
    ]
    /*const [allContact, setAllContact] = useState([])

    // Récupère tous les messages de contact
    useEffect(() => {
        axios.get(`${BASE_URL}/getAllContactMessage`)
            .then(res => setAllContact(res.data.result.result))
            .catch(err => console.log(err))
            
    },[])
    
   
    // Valide le changement d'état de la demande
    const submit = (index) => {
        // on fait du destructuring pour recuperer l'id et l'etat 
        const {id, etat} = allContact[index]
         axios.post(`${BASE_URL}/updateContactEtat`,{etat,id})
            .catch(err => console.log(err))
    }

    // Supprime un message
    const deleteContact = (id) => {
        axios.post(`${BASE_URL}/deleteContactMessage`, {id})
        .then(res => setAllContact(allContact.filter((e)=> e.id !== id)))
        .catch(err => console.log(err))
    }
    
    // Modifier l'état de la demande : traité/non traité
    const updateEtat = (index, etat) => {
        // on fait une copie de tous les messages de contact
        const contact = [...allContact]
        // on assigne le nouvel état au format number
        contact[index].etat = parseInt(etat)
        // on met a jour tous les contacts dans le state
        setAllContact([...contact])
    }*/
    
    return(
        <div className="messagerie">
            <h1 className="title">Demandes de contact</h1>
            
            <table className="admin-msg">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Objet</th>
                        <th>Message</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Email</th>
                        <th>Etat</th>
                        <th>Supprimer</th>
                    </tr>
                </thead>
               <tbody>
                     {allContact.map((demande,i)=> {
                        return(
                        <tr key={i} className={`etat-${demande.etat}`}>
                            <td>{demande.date}</td>
                            <td>{demande.objet}</td>
                            <td>{demande.message}</td>
                            <td>{demande.nom}</td>
                            <td>{demande.prenom}</td>
                            <td>{demande.email}</td>
                            {/*<td>
                                <select name="etat" onChange={(e) => updateEtat(i,e.target.value)} value={demande.etat}>
                		          <option value={0}>Demande Non Traitée</option>
                		          <option value={1}>Demande Traitée</option>
        		                </select>
        		              <button onClick={() => submit(i)}>Valider</button>
        		            </td>*/}
                            <td></td>
                            <td>
                                <FontAwesomeIcon className="icon-fatrash" icon={faTrash} />
                            </td>
                        </tr>
                      )
                   })} 
                   </tbody>
            </table>
        </div>
    )
}

export default AdminAllMessages
