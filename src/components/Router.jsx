import {Route, Routes} from "react-router-dom";
import Home from "./Home";
import Register from "./Register";
import Error404 from "./Error404";
import AboutUs from "./AboutUs";
import ContactUs from "./ContactUs";
import Profile from "./Profile";
import AdminAllUsers from "./AdminAllUsers";
import AdminAllMessages from "./AdminAllMessages";
import AddBeers from "./AddBeers";
import BrassAllBeers from "./BrassAllBeers";
import UpdateBeer from "./UpdateBeer";
import NosBrasseries from "./NosBrasseries";
import NosBieres from "./NosBieres";
import UpdateUser from "./UpdateUser";

const Router = () => {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/aboutUs" element={<AboutUs />} />
            <Route path="/contactUs" element={<ContactUs />} />
            <Route path="/profil" element={<Profile />} />
            <Route path="/allUsers" element={<AdminAllUsers />} />
            <Route path="/allMessages" element={<AdminAllMessages />} />
            <Route path="/addBeers" element={<AddBeers />} />
            <Route path="/brassAllBeers" element={<BrassAllBeers />} />
            <Route path="/updateBeer/:id" element={<UpdateBeer />} />
            <Route path="/updateUser/:id" element={<UpdateUser />} />
            <Route path="/aboutUs" element={<AboutUs />} />
            <Route path="/allBrasseries" element={<NosBrasseries/>} />
            <Route path="allBieres" element={<NosBieres/>} />

            <Route path="/*" element={<Error404 />} />
        </Routes>
    )
}

export default Router