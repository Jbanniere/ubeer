//import {useEffect, useState} from "react"
//import axios from "axios"
import { NavLink } from "react-router-dom"

const BrassAllBeers = () => {

    const allBeers = [
        {
            name : 'MyBeer',
            id : '1',
            description: 'Notre fantastique bière avec une pointe de citron',
            price: 4.50,
            img: './images/whiteBeer2.png'
        },
        {
            name : 'IPABeer',
            id : '2',
            description: 'Ipa au goût relevé et authentique',
            price: 4.50,
            img: './images/ipaBeer2.png'

        }
    ]

    //const [allBeers, ] = useState([])

    // Je récupère tous les produits
    /*useEffect(() => {
        axios.get(`${BASE_URL}/getallBeers`)
            .then(res => setallBeers(res.data.allBeers.result))
            .catch(err => console.log(err))
    },[])
    
    // Pour supprimer un produit
    const deleteProduct = (id) => {
        axios.post(`${BASE_URL}/deleteProduct`, {id})
        .then(res => setallBeers(allBeers.filter((e)=> e.id !== id)))
        .catch(err => console.log(err))
    }*/

    return(
        <div className="all-products-admin">
            <h1 className="title">Mes bières</h1>
            <button className="btn-addBeer"><NavLink to="/addBeers"> Ajouter une nouvelle Bière</NavLink></button>
            {allBeers.map((product,i) => {
                return(
                <div key={i}>
                    <table className="table-all-product" key={i}>
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>{product.name}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Titre</td>
                                <td>{product.description}</td>
                            </tr>
                            <tr>
                                <td>Prix </td>
                                <td>{product.price}€</td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td>
                                    <img className="all-product-img" src={product.img} alt={product.caption} />    
                                </td>
                            </tr>
                            <tr>
                                <td>Actions</td>
                                <td>
                                    <button className="btn-update"><NavLink to={`/updateBeer/${product.id}`}>Modifier les infos</NavLink></button>
                                    <button className="btn-delete" /*onClick={()=> deleteProduct(product.id)}*/>Supprimer</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                )
            })}
        </div>
        )
}

export default BrassAllBeers