import { Fragment } from "react";

const AboutUs = () => {
    return (
        <Fragment>
        <h1 className="title" >Notre Philosophie</h1>
        <div className="about-us-container">
            <div className="content">
                <img src="../images/logo 1.png" className="logo" alt="Ubeer Logo" />
                <div className="text-content">
                    <p>Ubeer propose un service de livraison de bières dans toute la France.</p>
                    <p>Un réseau de 300 brasseries.</p>
                    <p>Des livreurs disponibles et réactifs.</p>
                    <p>Des tarifs avantageux toute l&apos;année.</p>
                    <p>Chez Ubeer, nous croyons en la diversité et la qualité. C&apos;est pourquoi nous travaillons en étroite collaboration avec des brasseries artisanales locales et des producteurs indépendants pour vous offrir une sélection unique de bières. Que vous soyez amateur de lagers légères, de stouts robustes ou de IPAs aromatiques, nous avons quelque chose pour chaque palais.</p>
                    <p>Notre mission est de rendre la découverte de nouvelles bières aussi facile et agréable que possible. Avec notre service de livraison rapide et fiable, vous pouvez explorer le monde de la bière sans quitter le confort de votre maison. Rejoignez notre communauté de passionnés de bière et découvrez des saveurs inédites à chaque commande.</p>
                    <p>Nous organisons également des événements réguliers, tels que des dégustations en ligne et des visites de brasseries, pour enrichir votre expérience et vous permettre de rencontrer les brasseurs derrière vos bières préférées. Chez Ubeer, la bière n&apos;est pas seulement une boisson, c&apos;est une aventure.</p>
                </div>
                <img src="../images/brewery.png" className="brewery-image" alt="brewery" />
            </div>
        </div>
        </Fragment>
    );
}

export default AboutUs;
