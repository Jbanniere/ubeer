import { Fragment } from 'react';

const images = [
    '../images/blondBeer.png',
    '../images/ipaBeer2.png',
    '../images/ipaBeer2.png',
    '../images/whiteBeer2.png',
    '../images/blondBeer.png',
    '../images/ipaBeer2.png',
    '../images/ipaBeer2.png',
    '../images/whiteBeer2.png'
  ];

const NosBieres = () => {
  return (
    <Fragment>
        <h1 className='title'>Nos Bières</h1>
        <div className='filtre'>
          <label className="filtre-prix">Prix :</label>
              <select id="priceRange" name="prix">
                  <option value="all">All</option>
                  <option value="1-3">0 - 50</option>
                  <option value="3-5">51 - 100</option>
                  <option value="5+">101 - 200</option>
              </select>
          <label className="filtre-prix">Type de bières :</label>
          <select id="filtre-type" name="type">
              <option value="all">All</option>
              <option value="blonde">Blonde</option>
              <option value="ipa">IPA</option>
              <option value="blanche">Blanche</option>
          </select>
              
        </div>
        <div className="beer-grid">
            {images.map((image, index) => {
                return(
                    <card key={index}>
                        <img className ='image-grid' src={image}></img>
                        <div className='cart-bloc'>
                          <input className='qty' type="number" id="quantity" name="quantity" min="1"></input>
                          <button className='btn-panier'>    Ajouter au panier    </button>
                        </div>
                    </card>
                )
            })
        }
        </div>
    </Fragment>
  )
}

export default NosBieres