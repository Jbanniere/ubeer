import { Fragment } from 'react'
import { useState, useEffect } from 'react';

const NosBrasseries = () => {
    const [cities, setCities] = useState([]);
    const [selectedCity, setSelectedCity] = useState('');
    const [breweries, setBreweries] = useState([]);
    const [allBreweries, setAllBreweries] = useState([]);

    /*const allBrasseries = [
        {
            nom: 'Little Atlantique Brewery',
            street : 'rue de Chantenay',
            zip: 44400,
            city: 'Rezé',
            img: '../images/brass.png',
            description: 'Implanté depuis ...'
        },
        {
            nom: 'Little Atlantique Brewery',
            street : 'rue de Chantenay',
            zip: 44400,
            city: 'Rezé',
            img: '../images/brass.png',
            description: 'Implanté depuis ...'
        }
    ];*/

    // Simuler une requête GET pour récupérer la liste des villes
    const fetchCities = async () => {
        try {
            // Simuler un appel API avec un délai
            const response = await new Promise((resolve) => {
                setTimeout(() => {
                    resolve({
                        data: [
                            { id: 1, name: 'Paris' },
                            { id: 2, name: 'Lyon' },
                            { id: 3, name: 'Marseille' },
                            { id: 4, name: 'Toulouse' },
                            { id: 5, name: 'Nice' },
                            { id: 6, name: 'Nantes' }
                        ]
                    });
                }, 1000); // Simuler un délai de 1 seconde
            });

            setCities(response.data);
        } catch (error) {
            console.error('Error fetching cities:', error);
        }
    };

    // Simuler une requête GET pour récupérer la liste des brasseries
    const fetchBreweries = async () => {
        try {
            const response = await new Promise((resolve) => {
                setTimeout(() => {
                    resolve({
                        data: [
                            { id: 1, name: 'Brasserie 1', city: 'Paris', img: '../images/brass.png', },
                            { id: 2, name: 'Brasserie 2', city: 'Lyon', img: '../images/brewery.png', },
                            { id: 3, name: 'Brasserie 3', city: 'Marseille', img: '../images/brass.png', },
                            { id: 4, name: 'Brasserie 4', city: 'Toulouse', img: '../images/brewery.png', },
                            { id: 5, name: 'Brasserie 5', city: 'Nice',img: '../images/brass.png', },
                            { id: 6, name: 'Brasserie 6', city: 'Paris', img: '../images/brewery.png', },
                            { id: 7, name: 'Brasserie 7', city: 'Lyon', img: '../images/brass.png', },
                        ]
                    });
                }, 1000); // Simuler un délai de 1 seconde
            });

            setAllBreweries(response.data);
            setBreweries(response.data); // Initialiser les brasseries avec toutes les données
        } catch (error) {
            console.error('Error fetching breweries:', error);
        }
    };

    useEffect(() => {
        fetchCities();
        fetchBreweries();
    }, []);
    /*const handleChange = (event) => {
        const value = event.target.value;
        setSearchTerm(value);
    }*/

    useEffect(() => {
        if (selectedCity) {
            const filteredBreweries = allBreweries.filter(
                (brewery) => brewery.city === selectedCity
            );
            setBreweries(filteredBreweries);
        } else {
            setBreweries(allBreweries);
        }
    }, [selectedCity, allBreweries]);

  return (
    <Fragment>
        <h1 className='title'> Nos Brasseries</h1>
        <h1>Liste des Villes</h1>
            <label htmlFor="city">Choisissez une ville:</label>
            <select
                id="city"
                name="city"
                value={selectedCity}
                onChange={(e) => setSelectedCity(e.target.value)}
            >
            <option value="">Toutes les villes</option>
            {cities.map((city) => (
                <option key={city.id} value={city.name}>
                    {city.name}
                </option>
            ))}
            </select>
                {breweries.map((brasserie, i) => {
                    return(
                        <div key={i} className='brasserie'>
                            <div  className='brasserie-left'>
                                <img src={brasserie.img} alt=""/>
                            </div>
                            <div className='brasserie-right'>
                                <p className='nom-brass'> Nom : {brasserie.name}</p>
                                <p className='addr-brass'>Adresse : {brasserie.street} {brasserie.zip} {brasserie.city} </p>
                                <p className='desc-brass'>Description : {brasserie.description}</p>
                            </div> 
                        </div>
                    )
                })}
    </Fragment>
  )
}

export default NosBrasseries