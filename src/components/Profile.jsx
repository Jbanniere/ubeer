import { useState, useEffect } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import axios from 'axios';
import LoginButton from './LoginButton';

const Profile = () => {
  const { user, isAuthenticated, isLoading, getAccessTokenSilently } = useAuth0();
  const [userMetadata, setUserMetadata] = useState(null);
  const [profile, setProfile] = useState({
    name: '',
    prenom: '',
    street: '',
    zip: '',
    city: '',
    phone: '',
    email: '',
    img: ''
  });

  useEffect(() => {
    if (user) {
      setProfile({
        name: user.name || '',
        prenom: '',
        street: '',
        zip: '',
        city: '',
        phone: '',
        email: user.email || '',
        img: user.picture || ''
      });
    }
  }, [user]);

  useEffect(() => {
    const fetchUserMetadata = async () => {
      if (isAuthenticated) {
        try {
          const token = await getAccessTokenSilently();
          const response = await axios.get(`https://YOUR_AUTH0_DOMAIN/api/v2/users/${user.sub}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          console.log(user)
          setUserMetadata(response.data.user_metadata);
        } catch (e) {
          console.error(e);
        }
        console.log(user)
      }
    };

    fetchUserMetadata();
  }, [user, isAuthenticated, getAccessTokenSilently, user?.sub]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setProfile({ ...profile, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Here you would typically handle form submission, e.g., sending data to a server
    console.log('Profile submitted:', profile);
  };
  return (
    <div>
      {isAuthenticated ? (
        <div>
          <h1 className='title'>Mon Profil</h1>
          <div className='profil-infos'>
              <img src={profile.img} alt={user.name} />
              <div className='profil-contact'>
                <p>{profile.name} {profile.prenom}</p>
                <p>{profile.phone}</p>
                <p>{profile.email}</p>
                <p>{profile.street}</p>
                <p>{profile.zip} {profile.city}</p>
              </div>
          </div>
          <h3 className='title-profil'>User Metadata</h3>
          {userMetadata ? (
            <pre>{JSON.stringify(userMetadata, null, 2)}</pre>
          ) : (
            'No user metadata defined'
          )}

          <h2 className='title-profil'>Compléter/Modifier votre profil</h2>
          <form className ='form-profil' onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="nom">Nom</label>
              <input
                className='input-profil'
                type="text"
                id="name"
                name="name"
                value={profile.name}
                onChange={handleChange}
                placeholder={profile.name}
              />
            </div>
            <div className="form-group">
              <label htmlFor="prenom">Prénom</label>
              <input
                className='input-profil'
                type="text"
                id="prenom"
                name="prenom"
                value={profile.prenom}
                onChange={handleChange}
                placeholder={profile.prenom}
              />
            </div>
            <div className="form-group">
              <label htmlFor="street">N° et rue</label>
              <input
                className='input-profil'
                type="text"
                id="street"
                name="street"
                value={profile.street}
                onChange={handleChange}
                placeholder={profile.street}
              />
            </div>
            <div className="form-group">
              <label htmlFor="zip">ZIP</label>
              <input
                className='input-profil'
                type="text"
                id="zip"
                name="zip"
                value={profile.zip}
                onChange={handleChange}
                placeholder={profile.zip}
              />
            </div>
            <div className="form-group">
              <label htmlFor="city">City</label>
              <input
                className='input-profil'
                type="text"
                id="city"
                name="city"
                value={profile.city}
                onChange={handleChange}
                placeholder={profile.city}
              />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Numero tel</label>
              <input
                className='input-profil'
                type="text"
                id="phone"
                name="phone"
                value={profile.phone}
                onChange={handleChange}
                placeholder={profile.phone}
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">Adresse email</label>
              <input
                className='input-profil'
                type="email"
                id="email"
                name="email"
                value={profile.email}
                onChange={handleChange}
                placeholder={profile.email}
              />
            </div>
            <button className="btn-form" type="submit">Soumettre</button>
          </form>
        </div>
      ) : (
        <div className="profil-notauth">
          <p>Vous devez être connecté pour afficher votre profil :</p>
          <LoginButton />
        </div>
      )}
    </div>
  );
};

export default Profile;
